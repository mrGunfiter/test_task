#!/usr/bin/perl

use XML::LibXML;
use strict;
use warnings;
use Data::Dumper;

my $parser = new XML::LibXML;
my %option_array;
my %error_array;
use constant OPTION_NAME => 'option';
use constant PATTERN_NAME => 'pattern';
use constant HELP_NAME => 'help';
use constant CHECKED_VALUE => 'checked_value';

sub help()
{
    print("\n\tUSAGE: $0 some-xml-file.xml\n\n");
}

# упаковка в массив значений одной опции
sub get_all_patterns($)
{
    my @patterns = split('\|', $_[0]);
    foreach my $item (@patterns) {
        if (index($item, '=') != -1) {
            $item = substr($item, 0, index($item, '='));
        }
    }
    return @patterns;
}

# короткая опция совпадает с сокращенным вариантом длинной (см. документацию Getopt::long)
sub check_short($$)
{
    my $left = $_[0];
    my $rigth = $_[1];
    if (length($left) == 1) {
        if (substr($rigth, 0, 1) eq $left) {
            return $left;
        }
    }
    return '';
}

# проверка конфликтов опций
sub get_bad_pattern($$)
{
    my @patterns_left = get_all_patterns($_[0]);
    my @patterns_right = get_all_patterns($_[1]);
    foreach my $pl_item (@patterns_left) {
        foreach my $pr_item (@patterns_right) {
            if ($pl_item eq $pr_item) {
                return $pl_item;
            }
           if (check_short($pl_item, $pr_item) ne '') {
               return $pl_item;
           }
           if (check_short($pr_item, $pl_item) ne '') {
               return $pr_item;
           }
        }
    }
    return '';
}

# чтение xml
sub read_xml($)
{
    my $xmlp= $parser -> parse_file($_[0]);
    my $rootel = $xmlp -> getDocumentElement();
    my $elname = $rootel -> getName();
    my %work_arr; # 
    my @kids = $rootel -> childNodes();
    foreach my $child(@kids) {
        my $elname = $child -> getName();
        my @atts = $child -> getAttributes();
        my $pattern = '';
        my $help = '';
        foreach my $at (@atts) {
            if ($elname =~ OPTION_NAME) {
                my $name = $at -> getName();
                if ($name =~ PATTERN_NAME) {
                    $pattern = $at -> getValue();;
                }
                if ($name =~ HELP_NAME){
                    $help = $at -> getValue();;
                }
                if (($pattern ne '') and ($help ne '')) {
                    $work_arr{$pattern} = $help;
                }
            }
        }
    }
    return %work_arr;
}

# поиск конфликтующих паттернов
sub find_conflicts()
{
    my %err_array;
    foreach my $k1 (keys %option_array){
        foreach my $k2 (keys %option_array){
            if (($option_array{$k2} ne CHECKED_VALUE) and ($option_array{$k1} ne $option_array{$k2})){
                my $bad_pattern = get_bad_pattern($k1, $k2);
                if ($bad_pattern ne '') {
                    my $str_to_add;
                    my $str_exists;
                    if (exists $err_array{$bad_pattern}) {
                        $str_exists = $err_array{$bad_pattern};
                        if (index($str_exists, $option_array{$k1}) == -1) {
                            $str_to_add = $str_exists . "\t\t" . $option_array{$k1} . "\n";
                        } else {
                            $str_to_add = $str_exists;
                        }
                        if (index($str_exists, $option_array{$k2}) == -1) {
                            $str_to_add = $str_to_add . "\t\t" . $option_array{$k2} . "\n";
                        }
                    } else {
                        $str_to_add = "\t\t" . $option_array{$k1} . "\n\t\t" . $option_array{$k2} . "\n";
                    }
                    %err_array = (%err_array, $bad_pattern, $str_to_add);
                }
            }
        } # foreach my $k2 (keys %option_array)
        $option_array{$k1} = CHECKED_VALUE;
    } # foreach my $k1 (keys %option_array)
    return %err_array;
}
# ==================== Entry point ====================

if (!@ARGV) {
    help();
    exit(1);
} elsif (!-e $ARGV[0]) {
    print "file $ARGV[0] not found!\n";
    exit(1);
}

%option_array = read_xml($ARGV[0]);
%error_array = find_conflicts;

if (%error_array > 0) {
    print STDERR "Options conflict:\n";
    foreach my $err (keys %error_array){
        print STDERR "\t--" . $err . ": Could be:\n";
        print STDERR $error_array{$err};
    }
    exit(1);
};
exit(0);
