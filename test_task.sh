#!/bin/bash

TESTS_FILES=`ls . | grep "test.xml"`

for ONE_TEST_FILE in $TESTS_FILES
do
    echo -en "\n === work with $ONE_TEST_FILE ===\n"
    cat $ONE_TEST_FILE
    echo "============================"
    ./task.pl $ONE_TEST_FILE
done
